/*********************************************************************************************************************
Input Variables
*********************************************************************************************************************/

variable "home_sg" {
  type = list
}

variable "vpc" {
  type = string
}

variable "ec2_ssh_key" {
  type = string
}


/*********************************************************************************************************************
EC2 Host
*********************************************************************************************************************/


#! Creating Key
resource "aws_key_pair" "jb_key" {
  key_name = "jb_key"
  public_key = ""
}

#! Creating EC2
resource "aws_instance" "server" {
  ami = "ami-0dc2d3e4c0f9ebd18"
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.jb_key.key_name}"
  vpc_security_group_ids = var.home_sg

  tags = {
    Name = "Josh Test Server"
  }
}
