# Terraform
Terraform Repo

# Contents
---

## ec2 image
Building out an ec2 image in AWS to work with.

### Details
---
* Amazon Linux
* t2.micro


# Tasks
---

|    |      |
| -- | ---- |
|Task|Status|
| Create ec2| Pending |
| Create sg | Pending |
| Create role | Pending |

